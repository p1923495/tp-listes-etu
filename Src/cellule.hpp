#ifndef LIFAP6_LISTE_CELLULE_HPP
#define LIFAP6_LISTE_CELLULE_HPP

class Cellule {

  Cellule* ptrNext;
  Cellule* ptrPrev;
  public:
  int value;
  void setValue(int val);
  void setNext(Cellule* ptrNext);
  void setPrev(Cellule*ptrPrev);
  Cellule* getNext();
  Cellule* getPrev();
  int getVal();
  Cellule(int val);
} ;

#endif
