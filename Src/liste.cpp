#include "liste.hpp"

#include <iostream>
#include <cassert>

Liste::Liste()
{
  listSize = 0;
  celluleTete = nullptr;
  celluleQueue = nullptr;
}

Liste::Liste(const Liste &autre)
{
  listSize = 0;
  celluleTete = nullptr;
  celluleQueue = nullptr;
  Cellule *cible = autre.celluleQueue;
  if (cible == nullptr)
  {
    celluleTete = nullptr;
  }
  else
  {
    for (int i = 0; i < autre.taille(); i++)
    {
      ajouter_en_tete(cible->getVal());
      cible = cible->getNext();
    }
  }
}

Liste &Liste::operator=(const Liste &autre)
{
  while (celluleTete != nullptr)
  {
    supprimer_en_tete();
  }
  Cellule *cible = autre.celluleQueue;
  if (cible == nullptr)
  {
    celluleTete = nullptr;
    celluleQueue = nullptr;
    listSize = 0;
  }
  else
  {
    for (int i = 0; i < autre.taille(); i++)
    {
      ajouter_en_tete(cible->getVal());
      cible = cible->getNext();
    }
  }
  return *this;
}

Liste::~Liste()
{
  while (celluleTete != nullptr)
  {
    this->supprimer_en_tete();
  }
}

void Liste::ajouter_en_tete(int valeur)
{
  Cellule *newCell = new Cellule(valeur);
  newCell->setNext(celluleTete);
  newCell->setPrev(celluleQueue);
  if (celluleTete != nullptr)
  {
    celluleQueue->setNext(newCell);
    celluleTete->setPrev(newCell);
  }
  else
  {
    celluleQueue = newCell;
  }
  celluleTete = newCell;
  listSize++;
}

void Liste::ajouter_en_queue(int valeur)
{
  Cellule *newCell = new Cellule(valeur);
  newCell->setNext(celluleTete);
  newCell->setPrev(celluleQueue);
  if (celluleQueue != nullptr)
  {
    celluleQueue->setNext(newCell);
    celluleTete->setPrev(newCell);
  }
  else
  {
    celluleTete = newCell;
  }
  celluleQueue = newCell;
  listSize++;
}

void Liste::supprimer_en_tete()
{
  if (celluleTete != nullptr)
  {
    Cellule *bufferCell = celluleTete->getNext();
    delete celluleTete;
    if (listSize>1)
    {
      celluleTete = bufferCell;
      celluleTete->setPrev(celluleQueue);
      celluleQueue->setNext(celluleTete);
    }
    else
    {

      celluleQueue = nullptr;
      celluleTete = nullptr;
    }
    listSize--;
  }
}

Cellule *Liste::tete()
{
  return celluleTete;
}

const Cellule *Liste::tete() const
{
  return celluleTete;
}

Cellule *Liste::queue()
{
  return celluleQueue;
}

const Cellule *Liste::queue() const
{
  return celluleQueue;
}

int Liste::taille() const
{
  /* votre code ici */
  return listSize;
}

Cellule *Liste::recherche(int valeur)
{
  if (celluleTete == nullptr)
  {
    return nullptr;
  }
  int i = 0;
  Cellule *cible = celluleTete;
  while (i < listSize)
  {
    cible = cible->getNext();
    if (cible->getVal() == valeur)
    {
      return cible;
    }
    i++;
  }
  return nullptr;
}

const Cellule *Liste::recherche(int valeur) const
{
  if (celluleTete == nullptr)
  {
    return nullptr;
  }
  int i = 0;
  Cellule *cible = celluleTete;
  while (i < listSize)
  {
    cible = cible->getNext();
    if (cible->getVal() == valeur)
    {
      return cible;
    }
    i++;
  }
  return nullptr;
}

void Liste::afficher() const
{
  std::cout << "[ ";
  int i = 0;
  Cellule *cible = celluleTete;
  while (i < listSize)
  {
    std::cout << cible->getVal() << " ";
    cible = cible->getNext();
    i++;
  }
  std::cout << " ]"<<std::endl;
}
