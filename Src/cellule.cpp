#include "cellule.hpp"

void Cellule::setNext(Cellule* next){
    ptrNext=next;
}
void Cellule::setPrev(Cellule* prev){
    ptrPrev=prev;
}

void Cellule::setValue(int val){
    value=val;
}

int Cellule::getVal(){
    return value;
}

Cellule* Cellule::getNext(){
    return ptrNext;
}

Cellule* Cellule::getPrev(){
    return ptrPrev;
}

Cellule::Cellule(int val){
    value=val;
    ptrNext=nullptr;
    ptrPrev=nullptr;
}

